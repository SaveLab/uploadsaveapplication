const link = 'link of our website';

export const environment = {

  production: true,
  share: {
    message: 'You want to create perfect sales video easily ?',
    subject: 'This application is made for you!',
    url: link
  },
  firebaseConfig: {
    apiKey: 'AIzaSyDoytxbRXK9xb_y64LFHGzf_eHjX6WKQtY',
    authDomain: 'save-technologies-5e963.firebaseapp.com',
    databaseURL: 'https://save-technologies-5e963.firebaseio.com',
    projectId: 'save-technologies-5e963',
    storageBucket: 'save-technologies-5e963.appspot.com',
    messagingSenderId: '6744338013',
    appId: '1:6744338013:web:bf1d67cedc5c70ca63a7b4',
    measurementId: 'G-PQJZEZ5BDQ'
  }
};
