import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';


@Component({
  selector: 'app-sign',
  templateUrl: './sign.page.html',
  styleUrls: ['./sign.page.scss'],
})
export class SignPage implements OnInit {
  email: any;
  username: any;
  password: any;
  errorMessage: any;
  loading = false;

  constructor(private navCtrl: NavController,
              private authService: AuthenticationService,
              public afStore: AngularFirestore) {
  }

  ngOnInit() {
  }

  signUp() {
    this.authService.registerUser(this.email, this.password).then(res => {
      const user = {
        username: this.username,
        uid: res.user.uid,
        email: res.user.email,
        token: '',
        accepted: false,
        freeCar: false,
        freeCv: false,
        freeRe: false,
        pricingPlan: 'none',
        videoRendered: 0
      };
      this.afStore.collection('Users').doc(res.user.uid).set(user);
      this.authService.setUserDataLocal(user);
      this.goTo('home');
    }).catch(err => {
      this.errorMessage = err.message;
      throw err;
    });
  }

  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
  }

}
