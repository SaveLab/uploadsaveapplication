import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SplashScrenPage } from './splash-scren.page';

const routes: Routes = [
  {
    path: '',
    component: SplashScrenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SplashScrenPageRoutingModule {}
