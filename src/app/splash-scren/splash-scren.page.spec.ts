import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SplashScrenPage } from './splash-scren.page';

describe('SplashScrenPage', () => {
  let component: SplashScrenPage;
  let fixture: ComponentFixture<SplashScrenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplashScrenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SplashScrenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
