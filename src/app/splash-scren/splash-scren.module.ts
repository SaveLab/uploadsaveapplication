import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SplashScrenPageRoutingModule } from './splash-scren-routing.module';

import { SplashScrenPage } from './splash-scren.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SplashScrenPageRoutingModule
  ],
  declarations: [SplashScrenPage]
})
export class SplashScrenPageModule {}
