import { Component, OnInit } from '@angular/core';
import {NavController, LoadingController} from '@ionic/angular';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {first} from 'rxjs/operators';
@Component({
  selector: 'app-splash-scren',
  templateUrl: './splash-scren.page.html',
  styleUrls: ['./splash-scren.page.scss'],
})
export class SplashScrenPage implements OnInit {

  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private authService: AuthenticationService,
              public ngFireAuth: AngularFireAuth,
              public afStore: AngularFirestore) {}

  ngOnInit() {
    this.loadingApp();
  }

  async loadingApp() {
    this.ngFireAuth.authState.pipe(first()).subscribe(res => {
      if ( res ) {
        this.afStore.firestore.collection('Users').doc(res.uid).get()
            .then(docSnapshot => {
              if (!docSnapshot.exists) {
                this.goTo('login');
              } else {
                const user = docSnapshot.data();
                this.authService.setUserDataLocal(user);
                this.goTo('home');
              }
            });
      } else {
        this.goTo('login');
      }
    });
  }

  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
  }
}
