import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  username: any;
  password: any;
  connected: boolean;
  errorMessage: any;
  loading = false;


  constructor(
      private navCtrl: NavController,
      public router: Router,
      private authService: AuthenticationService,
      public afStore: AngularFirestore) {}

  ngOnInit(): void {
  }

  login() {
    this.loading = true;
    this.authService.signIn(this.username, this.password).then(res => {
      this.afStore.collection('Users').doc(res.user.uid).get().subscribe(doc => {
        if ( doc.exists ) {
          const user = doc.data();
          console.log(user);
          this.authService.setUserDataLocal(user);
          this.goTo('home');
        }
      });
      this.loading = false;
    }).catch(err => {
      this.loading = false;
      this.errorMessage = err.message;
      throw err;
    });
  }

  goTo(page: string) {
    this.navCtrl.navigateRoot('/' + page);
  }
}
