import { Component } from '@angular/core';
import {ModalsService} from '../shared/services/modals.service';
import {DataService} from '../shared/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor( public modaleService: ModalsService,
               public dataService: DataService) {}

  segmentChanged(ev: any) {
    this.dataService.displayAve = ev.target.value;
  }

}
