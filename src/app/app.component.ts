import {Component} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AngularFireAuth} from '@angular/fire/auth';
import {first} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthenticationService} from './shared/services/authentication.service';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private navCtrl: NavController,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public ngFireAuth: AngularFireAuth,
        public afStore: AngularFirestore,
        private authService: AuthenticationService,
    ) {
        this.initializeApp();
    }

    async loadingApp() {
        this.ngFireAuth.authState.pipe(first()).subscribe(res => {
            if (res) {
                this.afStore.firestore.collection('Users').doc(res.uid).get()
                    .then(docSnapshot => {
                        const user = docSnapshot.data();
                        this.authService.setUserDataLocal(user);
                        this.goTo('home');

                    });
            }
        });
    }

    goTo(page: string) {
        this.navCtrl.navigateRoot('/' + page);
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.loadingApp();
        });
    }
}
