import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {IsSignedInGuard} from './shared/guards/is-signed-in.guard';
import {IsNotSignedInGuard} from './shared/guards/is-not-signed-in.guard';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'splash-scren',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate: [
      IsSignedInGuard
    ]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
    canActivate: [
      IsNotSignedInGuard
    ]
  },
  {
    path: 'signup',
    loadChildren: () => import('./sign/sign.module').then( m => m.SignPageModule),
    canActivate: [
      IsNotSignedInGuard
    ]
  },
  {
    path: 'splash-scren',
    loadChildren: () => import('./splash-scren/splash-scren.module').then( m => m.SplashScrenPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./shared/modals/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'rendered-video-details',
    loadChildren: () => import('./shared/modals/rendered-video-details/rendered-video-details.module').then( m => m.RenderedVideoDetailsPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'render-cv',
    loadChildren: () => import('./shared/modals/render-cv/render-cv.module').then( m => m.RenderCvPageModule)
  },
  {
    path: 'render-car',
    loadChildren: () => import('./shared/modals/render-car/render-car.module').then( m => m.RenderCarPageModule)
  },
  {
    path: 'render-re',
    loadChildren: () => import('./shared/modals/render-re/render-re.module').then( m => m.RenderRePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
