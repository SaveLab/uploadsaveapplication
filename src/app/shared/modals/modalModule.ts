import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {ChooseAveComponent} from './choose-ave/choose-ave.component';
import {UploadHelpComponent} from './upload-help/upload-help.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [
        ChooseAveComponent,
        UploadHelpComponent

    ],
    providers: [],
    exports: [
        ChooseAveComponent,
        UploadHelpComponent
    ]
})

export class ModalModule {
}
