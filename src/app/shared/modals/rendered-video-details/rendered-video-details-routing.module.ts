import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenderedVideoDetailsPage } from './rendered-video-details.page';

const routes: Routes = [
  {
    path: '',
    component: RenderedVideoDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RenderedVideoDetailsPageRoutingModule {}
