import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenderedVideoDetailsPageRoutingModule } from './rendered-video-details-routing.module';

import { RenderedVideoDetailsPage } from './rendered-video-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenderedVideoDetailsPageRoutingModule
  ],
  declarations: [RenderedVideoDetailsPage]
})
export class RenderedVideoDetailsPageModule {}
