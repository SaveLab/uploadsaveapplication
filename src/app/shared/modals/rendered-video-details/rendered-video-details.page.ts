import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-rendered-video-details',
  templateUrl: './rendered-video-details.page.html',
  styleUrls: ['./rendered-video-details.page.scss'],
})
export class RenderedVideoDetailsPage implements OnInit {

  @Input() details: any;


  constructor(public modalController: ModalController) {}

  ngOnInit() {
  }

  close() {
    this.modalController.dismiss();
  }
}
