import {Component} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {LanguagesService} from '../../services/languages.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {

    isToggled = true;

    constructor(public router: Router,
                public modalController: ModalController,
                public authService: AuthenticationService,
                public socialSharing: SocialSharing,
                public languagesService: LanguagesService,
                private navCtrl: NavController) {
    }

    public notify() {

        if (this.isToggled) {
            localStorage.setItem('isToggled', 'true');
        } else {
            localStorage.setItem('isToggled', 'false');
        }
    }

    inquire() {

    }

    notice() {

    }

    howToUse() {

    }

    myProfil() {

    }

    privacyPolicy() {

    }

    share() {
        this.socialSharing.share(environment.share.message, environment.share.subject, null, environment.share.url);
    }


    signOut() {
        this.close();
        this.authService.signOut().then(() => {
            this.goTo('login');
            this.authService.deleteUserDataLocal();
        });
        localStorage.clear();
    }

    close() {
        this.modalController.dismiss();
    }

    goTo(page: string) {
        this.navCtrl.navigateRoot('/' + page);
    }
}
