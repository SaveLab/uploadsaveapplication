import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RenderCvPage } from './render-cv.page';

describe('RenderCvPage', () => {
  let component: RenderCvPage;
  let fixture: ComponentFixture<RenderCvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderCvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RenderCvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
