import {Component, Input, OnInit} from '@angular/core';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import {ModalsService} from '../../services/modals.service';
import {ToastService} from '../../services/toast.service';
import {DataService} from '../../services/data.service';
import {AuthenticationService} from '../../services/authentication.service';
import {MediaService} from '../../services/media.service';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
    selector: 'app-render-cv',
    templateUrl: './render-cv.page.html',
    styleUrls: ['./render-cv.page.scss'],
})
export class RenderCvPage implements OnInit {

    @Input() language;

    profile: string;
    competency: string;
    name: string;
    birthYear: string;
    education: string;
    major: string;
    address: string;
    yearOfExperience: number;
    expectedWorkingArea: string;
    desiredPosition: string;
    skill: string;
    knowledge: string;
    strongPoint: string;
    phoneNumber: string;

    constructor(public modalController: ModalController,
                public authService: AuthenticationService,
                public modalService: ModalsService,
                public dataService: DataService,
                public mediaService: MediaService,
                public toastService: ToastService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                public afSG: AngularFireStorage,
                public afS: AngularFirestore,
    ) {
    }


    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

    startRendering() {
        this.dataService.displayAve = 'cvs';
        this.close();
        this.toastService.videoStartRendering();
        //TODO Launch the rendering here by calling the AVE

    }

}
