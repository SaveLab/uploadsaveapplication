import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenderCvPageRoutingModule } from './render-cv-routing.module';

import { RenderCvPage } from './render-cv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenderCvPageRoutingModule
  ],
  declarations: [RenderCvPage]
})
export class RenderCvPageModule {}
