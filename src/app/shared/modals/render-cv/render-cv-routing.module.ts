import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenderCvPage } from './render-cv.page';

const routes: Routes = [
  {
    path: '',
    component: RenderCvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RenderCvPageRoutingModule {}
