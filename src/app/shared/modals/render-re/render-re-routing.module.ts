import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenderRePage } from './render-re.page';

const routes: Routes = [
  {
    path: '',
    component: RenderRePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RenderRePageRoutingModule {}
