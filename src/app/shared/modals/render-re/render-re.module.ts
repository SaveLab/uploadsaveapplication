import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenderRePageRoutingModule } from './render-re-routing.module';

import { RenderRePage } from './render-re.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenderRePageRoutingModule
  ],
  declarations: [RenderRePage]
})
export class RenderRePageModule {}
