import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RenderRePage } from './render-re.page';

describe('RenderRePage', () => {
  let component: RenderRePage;
  let fixture: ComponentFixture<RenderRePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderRePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RenderRePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
