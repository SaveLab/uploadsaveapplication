import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ModalsService} from '../../services/modals.service';
import {ToastService} from '../../services/toast.service';
import {DataService} from '../../services/data.service';

@Component({
    selector: 'app-render-re',
    templateUrl: './render-re.page.html',
    styleUrls: ['./render-re.page.scss'],
})
export class RenderRePage implements OnInit {

    @Input() language;

    title: string;
    thumbnail: string;
    interiorBack: string;
    interiorFront: string;
    left: string;
    trunk: string;
    right: string;
    bonet: string;
    registration: string;
    memo: string;

    constructor(public modalController: ModalController,
                public modalService: ModalsService,
                public dataService: DataService,
                public toastService: ToastService) {
    }

    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

    startRendering() {
        this.dataService.displayAve = 'realestate';
        this.close();
        this.toastService.videoStartRendering();
        //TODO Launch the rendering here by calling the AVE

    }

}
