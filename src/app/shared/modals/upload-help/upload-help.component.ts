import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-upload-help',
  templateUrl: './upload-help.component.html',
  styleUrls: ['./upload-help.component.scss'],
})
export class UploadHelpComponent implements OnInit {
@Input() image;
@Input() title;
  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  close() {
    this.modalController.dismiss();
  }
}
