import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ModalsService} from '../../services/modals.service';
import {LanguagesService} from '../../services/languages.service';

@Component({
    selector: 'app-choose-ave',
    templateUrl: './choose-ave.component.html',
    styleUrls: ['./choose-ave.component.scss'],
})
export class ChooseAveComponent implements OnInit {

    field = '';
    language = '';

    constructor(public modalController: ModalController,
                public modaleService: ModalsService,
                public languagesService: LanguagesService) {

    }

    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

    render() {

        if (!this.field || !this.language) {
            console.log('pls fill up the selectors');
        }
        if (this.field === 'cars' && this.language) {
            this.modaleService.showRenderCarModal(this.language);
            this.close();
        }
        if (this.field === 'realestate' && this.language) {
            this.modaleService.showRenderReModal(this.language);
            this.close();
        }
        if (this.field === 'cvs' && this.language) {
            this.modaleService.showRenderCvModal(this.language);
            this.close();
        }
    }


    selectAveField(event) {
        this.field = event.target.value;
    }

    selectAveLanguage(event) {
        this.language = event.target.value;
    }

}
