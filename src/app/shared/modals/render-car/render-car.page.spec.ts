import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RenderCarPage } from './render-car.page';

describe('RenderCarPage', () => {
  let component: RenderCarPage;
  let fixture: ComponentFixture<RenderCarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderCarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RenderCarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
