import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenderCarPage } from './render-car.page';

const routes: Routes = [
  {
    path: '',
    component: RenderCarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RenderCarPageRoutingModule {}
