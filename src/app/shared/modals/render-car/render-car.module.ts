import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenderCarPageRoutingModule } from './render-car-routing.module';

import { RenderCarPage } from './render-car.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenderCarPageRoutingModule
  ],
  declarations: [RenderCarPage]
})
export class RenderCarPageModule {}
