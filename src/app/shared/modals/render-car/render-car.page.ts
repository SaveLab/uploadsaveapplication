import {Component, Input, OnInit} from '@angular/core';
import {ModalController, LoadingController, AlertController} from '@ionic/angular';
import {ModalsService} from '../../services/modals.service';
import {ToastService} from '../../services/toast.service';
import {DataService} from '../../services/data.service';
import {MediaService} from '../../services/media.service';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import * as firebase from 'firebase';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
    selector: 'app-render-car',
    templateUrl: './render-car.page.html',
    styleUrls: ['./render-car.page.scss'],
})


export class RenderCarPage implements OnInit {

    @Input() language;


    briefInfo: string;
    optionMain: string;
    engineType: string;
    engineSize: string;
    fuelEfficiency: string;
    hoursePower: string;
    touque: string;
    driveWheel: string;
    transmission: string;
    carName: string;
    productionYear: number;
    price: number;
    phoneNumer: string;
    mileAge: string;
    accident: string;
    fuelType: string;
    location: string;
    interiorBack: string;
    interiorFront: string;
    left: string;
    trunk: string;
    right: string;
    bonet: string;
    registration: string;

    OrderedFiles = [];


    constructor(public modalController: ModalController,
                public authService: AuthenticationService,
                public modalService: ModalsService,
                public dataService: DataService,
                public mediaService: MediaService,
                public toastService: ToastService,
                public loadingController: LoadingController,
                public alertController: AlertController,
                public afSG: AngularFireStorage,
                public afS: AngularFirestore,
    ) {
    }


    ngOnInit() {
    }

    close() {
        this.modalController.dismiss();
    }

    startRendering(field: string) {
        this.dataService.displayAve = 'cars';
        this.close();
        this.toastService.videoStartRendering();
        //TODO Launch the rendering here by calling the AVE

    }


}
