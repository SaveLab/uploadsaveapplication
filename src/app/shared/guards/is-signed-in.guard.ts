import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {NavController} from '@ionic/angular';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class IsSignedInGuard implements CanActivate {

    constructor(private authService: AuthenticationService,
                private navCtrl: NavController) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ( !this.authService.isLoggedIn ) {
            this.navCtrl.navigateRoot('/login');
        }
        return this.authService.isLoggedIn;
    }

}
