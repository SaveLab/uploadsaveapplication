import {Injectable, OnInit} from '@angular/core';
import {AuthenticationService} from './authentication.service';


@Injectable({
    providedIn: 'root'
})
export class LanguagesService {
    language: any;
    languages = {
        en: {
            lang: 'Language',
            signIn: 'Sign In',
            signUp: 'Sign Up',
            signOut: 'Log Out',
            myAccount: 'My account',
            share: 'Share',
            profile: 'My profile',
            subProfile: 'Edit profile',
            korean: 'Korean',
            english: 'English',
            french: 'French',
            settings: 'Settings',
            renderVideo: 'Render a Video',
            aveOptions: 'Select the Option for your videos',
            selectOne: 'Select One',
            field: 'Field',
            continue: 'Continue'

        },
        fr: {
            lang: 'Langue',
            signIn: 'Se connecter',
            signUp: 'S inscrire',
            signOut: 'Se deconnecter',
            myAccount: 'Mon compte',
            share: 'Partager',
            profile: 'Mon profile',
            subProfile: 'Modifier son profil',
            korean: 'Coréen',
            english: 'Anglais',
            french: 'Francais',
            settings: 'Paramètres',
            renderVideo: 'Créer une video',
            aveOptions: 'Selectionnez les options pour votre vidéo',
            selectOne: 'Selectioner',
            field: 'Domaine',
            continue: 'Continuer'

        },
        kr: {
            signIn: 'Sign In',
            signUp: 'Sign Up'
        }
    };

    constructor(public authService: AuthenticationService) {

        if (this.authService.isLoggedIn) {
            this.language = this.authService.userData.language;
        } else {
            this.language = 'en';
        }
        console.log('la langue est: ' + this.language);
    }

    selectLanguage(event) {

        this.language = event.target.value;
        if (this.authService.isLoggedIn) {
            this.authService.userData.language = this.language;
            this.authService.updateUserDataFirebase();
        }

    }

    mode() {
        return this.languages[this.language];
    }
}
