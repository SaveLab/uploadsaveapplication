import {Injectable} from '@angular/core';
import {Platform, ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {environment} from 'src/environments/environment';
import {AuthenticationService} from 'src/app/shared/services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    displayAve = 'cars';

    videosRealEstate = [];
    videosCvs = [];
    videosCars = [];

    constructor(
        public router: Router,
        public aFS: AngularFirestore,
        public platform: Platform,
        public toastController: ToastController,
        public authService: AuthenticationService) {

        this.aFS.collection('Users')
            .doc(authService.userData.uid)
            .collection('renderedVideo')
            .doc('realEstate')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosRealEstate.push(video);
                });
            });
        this.aFS.collection('Users')
            .doc(authService.userData.uid)
            .collection('renderedVideo')
            .doc('cv')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosCvs.push(video);
                });
            });
        this.aFS.collection('Users')
            .doc(authService.userData.uid)
            .collection('renderedVideo')
            .doc('car')
            .collection('data')
            .valueChanges()
            .subscribe(videos => {
                videos.forEach(video => {
                    this.videosCars.push(video);
                });
            });

    }

    gotoSegment(name: string) {
        this.displayAve = name;
    }


}
