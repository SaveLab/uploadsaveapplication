import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {SettingsPage} from '../modals/settings/settings.page';
import {ChooseAveComponent} from '../modals/choose-ave/choose-ave.component';
import {RenderCarPage} from '../modals/render-car/render-car.page';
import {RenderRePage} from '../modals/render-re/render-re.page';
import {RenderCvPage} from '../modals/render-cv/render-cv.page';
import {RenderedVideoDetailsPage} from '../modals/rendered-video-details/rendered-video-details.page';
import {UploadHelpComponent} from '../modals/upload-help/upload-help.component';

@Injectable({
    providedIn: 'root'
})
export class ModalsService {

    constructor(public modalController: ModalController) {
    }


    async showSettingsModal() {
        const modal = await this.modalController.create({
            component: SettingsPage
        });
        await modal.present();

        return await modal.onWillDismiss();
    }

    async showChooseAveModal() {
        const modal = await this.modalController.create({
            component: ChooseAveComponent
        });
        await modal.present();

        return await modal.onWillDismiss();
    }


    async showRenderCarModal(language: any) {
        const modal = await this.modalController.create({
            component: RenderCarPage,
            componentProps: {
                language
            }
        });
        await modal.present();

        return modal.onWillDismiss();
    }
    async showRenderCvModal(language: any) {
        const modal = await this.modalController.create({
            component: RenderCvPage,
            componentProps: {
                language
            }
        });
        await modal.present();

        return modal.onWillDismiss();
    }
    async showRenderReModal(language: any) {
        const modal = await this.modalController.create({
            component: RenderRePage,
            componentProps: {
                language
            }
        });
        await modal.present();

        return modal.onWillDismiss();
    }
    async showRenderedVideoDetailsModal(details: any) {
        const modal = await this.modalController.create({
            component: RenderedVideoDetailsPage,
            componentProps: {
                details
            }
        });
        await modal.present();

        return modal.onWillDismiss();
    }
    async showUploadHelpModal(image: any, title: any) {
        const modal = await this.modalController.create({
            component: UploadHelpComponent,
            componentProps: {
                image,
                title
            }
        });
        await modal.present();

        return modal.onWillDismiss();
    }
}



