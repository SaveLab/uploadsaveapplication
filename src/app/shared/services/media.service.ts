import {Injectable} from '@angular/core';
import {LoadingController, AlertController, NavController, ModalController} from '@ionic/angular';
import {AuthenticationService} from './authentication.service';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import * as firebase from 'firebase';
import {ToastService} from './toast.service';


@Injectable({
    providedIn: 'root'
})
export class MediaService {
    clips = [];



    constructor(
        public loadingController: LoadingController,
        public alertController: AlertController,
        private navCtrl: NavController,
        public afSG: AngularFireStorage,
        public toastService: ToastService,
        public modalController: ModalController,
        public afS: AngularFirestore,
        public authService: AuthenticationService,
    ) {
    }

    async selectFile(event: FileList, clip: string) {
        if (!this.clips) {
            this.clips = [{
                name: clip,
                file: event.item(0)
            }];
        } else {
            this.clips.push({
                name: clip,
                file: event.item(0)
            });
        }

    }


    async uploadFile(field, max: number, lg: string, freeTrial: boolean, nameofField: string) {
        if (freeTrial){
            this.toastService.freeTrialAlreadyDone(nameofField);
        }else{

            if (this.clips.length < max || !lg) {
                this.toastService.fillAllInput();
            } else {
                const Files = {
                    language: lg
                };
                const loading = await this.loadingController.create({
                    cssClass: 'my-custom-class',
                    message: 'Upload video clip...',
                    backdropDismiss: false,
                    mode: 'ios'
                });


                await loading.present();
                const time = firebase.firestore.Timestamp.now().toMillis().toString();
                for (let i = 0; i <= max; i++) {
                    const value = this.clips[i].name;
                    const path = field + '/' + this.authService.userData.uid + '/ ' + time + '/ ' + value;
                    const customMetadata = {app: 'clip video' + value};
                    const fileRef = this.afSG.ref(path);
                    this.afSG.upload(path, this.clips[i].file, {customMetadata}).then(() => {
                        fileRef.getDownloadURL().subscribe(res => {
                            Files[value] = res;
                            this.afS.collection('Users')
                                .doc(this.authService.userData.uid)
                                .collection('renderedVideo')
                                .doc(field).collection('data')
                                .doc(time)
                                .set(Files);
                            loading.dismiss();
                        }, error => {
                            loading.dismiss();
                        });
                    });
                }
                this.close();

            }
        }



    }
    goTo(page){
        this.navCtrl.navigateRoot('/' + page);
    }

    close() {
        this.modalController.dismiss();
    }
}
